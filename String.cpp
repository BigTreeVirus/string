﻿

#include <iostream>
#include <string>

void main()
{
    std::string String = "Hello world";

    std::cout << String << "\n";
    std::cout << "Length: " << String.length() << "\n";
    std::cout << "First letter: " << String.front() << "\n";
    std::cout << "Last letter: " << String.back() << "\n";

}

